import React, { Component } from 'react';
import Bill from './Bill'
import Tip from './Tip'
import People from './People'

class Input extends Component {
  render() {
    const { BillAmt, tipAmt, tipPercentage, people } = this.props.billData;
    const { billInput, checkBoxInput, customBoxInput, peopleCount } = this.props;
    return <React.Fragment>
      <Bill BillAmt={BillAmt} handleBillInput={billInput} />
      <Tip tipAmt={tipAmt} tipPercentage={tipPercentage} handleCheckBoxInput={checkBoxInput} handleCustomBoxInput={customBoxInput} />
      <People people={people} handlePeopleCount={peopleCount} />
    </React.Fragment>
  }
}

export default Input;
