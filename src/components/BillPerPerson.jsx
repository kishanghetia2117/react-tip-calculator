import React from 'react';

function BillPerPerson(props) {
  const { total } = props;
  const divStyle = {
    display: 'flex',
  }
  return <div style={divStyle}>
    <div >
      <div>Total</div>
      <div>/ person</div>
    </div>
    <h2>{'$' + total.toFixed(2)}</h2>
  </div>
}

export default BillPerPerson;
