import React, { Component } from 'react';

class People extends Component {
  render() {
    const { people, handlePeopleCount } = this.props
    return <div>
      <label>Number of People</label>
      {/* <div>Can't be zero or less</div> */}
      <input type="number" id="people-count" placeholder="1" min="1" onChange={handlePeopleCount} />
    </div>
  }
}

export default People;
