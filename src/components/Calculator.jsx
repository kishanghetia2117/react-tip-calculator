import React, { Component } from 'react';
import Input from './Input'
import Output from './Output'

class Calculator extends Component {
  state = {
    BillAmt: 0,
    tipAmt: 0.15,
    tipPercentage: [
      { id: 1, value: 5, state: '' },
      { id: 2, value: 10, state: '' },
      { id: 3, value: 15, state: 'checked' },
      { id: 4, value: 25, state: '' },
      { id: 5, value: 50, state: '' }
    ],
    people: 1,
    tipValue: 0,
    total: 0,

  }
  getBillInput = (event) => {
    this.setState({
      BillAmt: event.target.value
    }, () => this.calculateTip())
  }

  getCheckedinput = (event) => {
    const percentageBoxes = this.state.tipPercentage.map(item => {
      if (parseInt(event.target.value) === item.value) {
        item.state = 'checked'
      } else {
        item.state = ''
      }
      return item
    })
    this.setState({
      tipAmt: event.target.value / 100,
      tipPercentage: percentageBoxes
    }, () => this.calculateTip())
  }
  getCustomBoxInput = (event) => {
    const percentageBoxes = this.state.tipPercentage.map(item => {
      item.state = ''
      return item
    })
    this.setState({
      tipAmt: event.target.value / 100,
      tipPercentage: percentageBoxes
    }, () => this.calculateTip())
  }
  getPeopleCount = (event) => {
    this.setState({
      people: event.target.value
    }, () => this.calculateTip())
  }
  calculateTip = () => {
    console.clear()
    const { BillAmt, tipAmt, people, tipValue, total } = this.state
    if (BillAmt > 0 && tipAmt >= 0 && people > 0) {
      let calTipValue = BillAmt * tipAmt / people;
      let calTotal = (BillAmt / people) + calTipValue;
      console.log(BillAmt, tipAmt, people, calTipValue, calTotal)
      this.setState({
        tipValue: calTipValue,
        total: calTotal
      })
    } else {
      this.setState({
        tipValue: 0,
        total: 0
      })
    }
  }

  rstValue = () => {
    this.setState({
      BillAmt: '',
      tipAmt: 0.15,
      tipPercentage: [
        { id: 1, value: 5, state: '' },
        { id: 2, value: 10, state: '' },
        { id: 3, value: 15, state: 'checked' },
        { id: 4, value: 25, state: '' },
        { id: 5, value: 50, state: '' }
      ],
      people: 1,
      tipValue: 0,
      total: 0,
    })
  }
  render() {

    return <React.Fragment >
      <Input
        billData={this.state}
        billInput={this.getBillInput}
        checkBoxInput={this.getCheckedinput}
        customBoxInput={this.getCustomBoxInput}
        peopleCount={this.getPeopleCount}
      />
      <Output tipValue={this.state.tipValue} total={this.state.total} rst={this.rstValue} />
    </React.Fragment >;
  }
}

export default Calculator;
