import React from 'react';

function TipButton(props) {
  const { value, state } = props.buttonData
  return <label>
    <input type="checkbox" value={value} checked={state} onChange={props.handlecheckBoxInput} />
    <span>{value + '%'}</span>
  </label>
}

export default TipButton;
