import React, { Component } from 'react';
import TipButton from './TipButton'


class Tip extends Component {
  render() {
    const { tipPercentage, tipAmt, handleCheckBoxInput, handleCustomBoxInput } = this.props;
    return <div>
      <label>Select Tip %</label>
      <div>
        {tipPercentage.map((item, index) => <TipButton buttonData={item} key={item.id} handlecheckBoxInput={handleCheckBoxInput} />)}
        <label>
          <input
            type="number"
            id="tip-amt"
            placeholder="Custom"
            min="0"
            onChange={handleCustomBoxInput}

          />
        </label>
      </div>
    </div>
  }
}

export default Tip;