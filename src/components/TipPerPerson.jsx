import React from 'react';

function TipPerPerson(props) {
  const { tipValue } = props;
  const divStyle = {
    display: 'flex',
  }
  return <div style={divStyle}>
    <div >
      <div >Tip Amount</div>
      <div >/ person</div>
    </div>
    <h2 >{'$' + tipValue.toFixed(2)}</h2>
  </div>
}

export default TipPerPerson;
