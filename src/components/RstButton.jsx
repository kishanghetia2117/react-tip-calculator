import React from 'react';

function RstButton(props) {
  const { rst } = props
  return <div>
    <input type="reset" value="Reset" onClick={rst} />
  </div>
}

export default RstButton;
