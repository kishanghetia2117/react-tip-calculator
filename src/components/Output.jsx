import React, { Component } from 'react';
import TipPerPerson from './TipPerPerson'
import BillPerPerson from './BillPerPerson'
import RstButton from './RstButton'

class Input extends Component {
  render() {
    const { tipValue, total, rst } = this.props;
    console.log(tipValue, total)
    const divStyle = {
      display: 'flex',
    }
    return <div style={divStyle}>
      <TipPerPerson tipValue={tipValue} />
      <BillPerPerson total={total} />
      <RstButton rst={rst} />
    </div>
  }
}

export default Input;