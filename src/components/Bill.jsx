import React, { Component } from 'react';

class Bill extends Component {
  render() {
    const { BillAmt, handleBillInput } = this.props;

    return <div>
      <label>Bill</label>
      <input type="number" placeholder="$ 0.0" min="0" onChange={handleBillInput} />
    </div>
  }
}

export default Bill;
