import logo from './components/images/logo.svg'
import Calculator from './components/Calculator';
import './App.css';

function App() {
  return (
    <div className="App-container">
      <div>
        <img src={logo} alt="logo" />
      </div>
      <Calculator />
    </div>
  );
}

export default App;
